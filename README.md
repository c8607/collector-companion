<div align="center">![Future Collector Companion Image](./images/ccnew.png)</div>

# **Collector Companion**

## **Introduction**

*Collector Companion will allow the gaming community to keep track of a variety of in-game features that is challenges, goals, and in-game items. This includes adding what they have found to the application and applying images and text for more information. Other features of the application will allow end-users to publish their game collection for online community viewing and for others to use to keep track of that similar collection.*

## **Executive Summary**

The project will be taken through the phases of the software development life cycle and updates will be posted as the phases of development are approached. The documents below will contain information on initiation, planning, analysis, designing, and implementation project phases. 

## **Current Software Development Phase:**

- [x] Requirements Gathering: 
[Project Proposal](./docs/ccprojectproposal.md) | [Business Problem Scenario](./docs/ccbusinessproblemscenario.md) | [Microsoft Project Plan](./docs/ccmsprojectplan.pdf) | [Software Requirements Specification (SRS)](./docs/ccsrs.md) 
		
		
-  [x] Planning: 
[Identified Use Cases](./docs/cclistofusecases.md) | [Use Case Descriptions](./docs/ccbasicusecasedescriptions.md) | [Use Case Diagram](./docs/cc1usecasediagram.pdf) | [Class Diagram](./docs/cc1classdiagram.pdf) | [View of Participating Classes Matrix](./docs/cc1vopcmatrix.pdf)
	 
	 
-  [x] Analysis: 
[SRS with Software System Attributes](./docs/ccsrs.md) | [Standard Test Plan](./docs/ccstandardtestplan.md)
	
	 
- [x] Designing: 
[Software Architecture Description](./docs/design/SoftwareArchitectureDescription.md) -- [Static View](./docs/design/ccstaticviewA.png) & [Dynamic View](./docs/design/ccdynamicview.png) | [Software Design Description](./docs/design/SoftwareDesignDescription.md) -- [Static View](./docs/design/ccoodstaticview.png) & [Dynamic View](./docs/design/ccooddynamicview.png)


- [ ] Implementation: 
[Source Code](./src) | [Test Cases](./docs/implementation/cctestcases.md) | [Test Report Summary](./docs/implementation/ccinitialtestreport.md) | [Error Documentation](./docs/errordocumentation.md) 
	
	
## **Conclusion**

- The Collector Companion application will go through further development to ensure all features are working for software release and download.

<hr>

# Return to [Repository Page](https://gitlab.com/kghernandez9/repositorypage).