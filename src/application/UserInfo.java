package application;


public class UserInfo {

	//Attribute
	private String userSignature;


	//Constructors
	public UserInfo() {
		userSignature = "Unknown";
	}

	public UserInfo(String userSignature) {
		this.setUserSignature(userSignature);
	}

	public void setUserSignature(String userSignature) {//Receives user's signature and assigns it
		if ( userSignature.length() > 0 )
			this.userSignature = userSignature;
		else
			userSignature = "Uknown";
	}

	public String getUserSignature(){//Returns user's signature
		return userSignature;
	}
}
