package application;


public class Subcategory {

	//Attributes
	private String subcategoryTitle;
	private String subcategoryComments;
	private Boolean isSubcategoryComplete;

	//Constructors
	public Subcategory() {
		subcategoryTitle = "N/A";
		subcategoryComments = "N/A";
		isSubcategoryComplete = false;
	}

	public Subcategory(String subcategoryTitle, String subcategoryComments, Boolean isSubcategoryComplete) {
		this.setSubcategoryTitle(subcategoryTitle);
		this.setSubcategoryComments(subcategoryComments);
		this.setIsSubcategoryComplete(isSubcategoryComplete);

	}

	//Setters and Getters
	public void setSubcategoryTitle(String subcategoryTitle) {//Receives sub-category title and assigns it
		if (subcategoryTitle.length() > 0)
			this.subcategoryTitle = subcategoryTitle;
		else
			this.subcategoryTitle = "Unkonwn category title.";
	}

	public String getSubcategorytitle() {//Returns sub-category title
		return subcategoryTitle;
	}

	public void setSubcategoryComments(String subcategoryComments) {//Receives sub-category comments and assigns it
		this.subcategoryComments = subcategoryComments;
	}

	public String getSubcategoryComments() {//Returns sub-category comments
		return subcategoryComments;
	}

	public void setIsSubcategoryComplete(Boolean isSubcategoryComplete) {//Receives input from check-box status and assigns off/on
		if (isSubcategoryComplete = true)
			this.isSubcategoryComplete = true;
		else
			this.isSubcategoryComplete = false;
	}

	public Boolean getIsCategoryComplete() {//Returns sub-category complete status
		return isSubcategoryComplete;
	}
}//End of Sub-Category Class
