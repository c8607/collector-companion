package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class CollectorCompanionGUISceneController {
	@FXML
	private AnchorPane anchorPaneBackground;
	@FXML
	private Button btnMyCollections;
	@FXML
	private Button btnCommunityCollections;
	@FXML
	private ImageView imageViewLogo;

	// Event Listener on Button[#btnMyCollections].onAction
	@FXML
	public void btnMyCollectionsClicked(ActionEvent event) throws IOException {
		//Proceed to My Collections Scene
		Parent tableViewParent = FXMLLoader.load(getClass().getResource("/application/MyCollectionsGUI.fxml"));
		Scene tabletViewScene = new Scene(tableViewParent);

		//Get the stage information for current window
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(tabletViewScene);
		window.show();

	}//end of btnMyCollections

	// Event Listener on Button[#btnCommunityCollections].onAction
	@FXML
	public void btnCommunityCollectionsClicked(ActionEvent event) throws IOException {
		// Proceed to Community Collections
		Parent tableViewParent = FXMLLoader.load(getClass().getResource("/application/CommunityCollectionsGUI.fxml"));
		Scene tabletViewScene = new Scene(tableViewParent);

		//Get the stage information for current window
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(tabletViewScene);
		window.show();	

	}//end of btnCommunityCollections
}//end of CollectorCompanionGUISceneController
