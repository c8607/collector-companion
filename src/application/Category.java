package application;


public class Category {

	//Attributes
	private String categoryTitle;
	private String categoryComments;
	private boolean isCategoryComplete;

	//Constructors
	public Category() {
		categoryTitle = "N/A";
		categoryComments = "N/A";
		isCategoryComplete = false;
	}

	public Category(String categoryTitle, String categoryComments, Boolean isCategoryComplete) {
		this.setCategoryTitle(categoryTitle);
		this.setCategoryComments(categoryComments);
		this.setIsCategoryComplete(isCategoryComplete);

	}

	//Setters and Getters
	public void setCategoryTitle(String categoryTitle) {//Receives category title and assigns it
		if (categoryTitle.length() > 0)
			this.categoryTitle = categoryTitle;
		else
			this.categoryTitle = "Unkonwn category title.";
	}

	public String getCategorytitle() {//Returns category title
		return categoryTitle;
	}

	public void setCategoryComments(String categoryComments) {//Receives category comments and assigns it
		this.categoryComments = categoryComments;
	}

	public String getCategoryComments() {//Returns category comments
		return categoryComments;
	}

	public void setIsCategoryComplete(Boolean isCategoryComplete) {//Receives input from check-box component and assigns off/on
		if (isCategoryComplete = true)
			this.isCategoryComplete = true;
		else
			this.isCategoryComplete = false;
	}

	public Boolean getIsCategoryComplete() {//Returns category complete status
		return isCategoryComplete;
	}

}//end of Category class
