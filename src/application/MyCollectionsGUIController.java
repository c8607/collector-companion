package application;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class MyCollectionsGUIController {
	@FXML
	private Label textViewMyCollections;
	@FXML
	private Button btnReturn;

	// Event Listener on Button[#btnReturn].onAction
	@FXML
	public void btnReturnClicked(ActionEvent event) throws IOException {
		//Proceed to CollectorCompanionGUI Scene
		Parent tableViewParent = FXMLLoader.load(getClass().getResource("/application/CollectorCompanionGUIScene.fxml"));
		Scene tabletViewScene = new Scene(tableViewParent);

		//Get the stage information for current window
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(tabletViewScene);
		window.show();
	}//End of method btnReturnedClicked
	
	@FXML
    void onMouseDragged(MouseEvent event) {

    }//end of method onMouseDragged

    @FXML
    void onMousePressed(MouseEvent event) {

    }//end of method onMousePressed
	
}
