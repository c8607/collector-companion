package application;

public class Image {

	//Attributes
	private int imageWidth;
	private int imageLength;
	private int imageHint;

	//Constructors
	public Image(){
		imageWidth = 0;
		imageLength = 0;
		imageHint = 0;
	}

	public Image(int imageWidth, int imageLength, int imageHint){
	this.setImageWidth(imageWidth);
	this.setImageLength(imageLength);
	this.setImageHint(imageHint);
	}

	//behaviors
	//public void setImage() {
	//};

	//Setters and Getters
	public void setImageWidth(int imageWidth) { //Receives width value and assigns
		if (imageWidth > 0)
			this.imageWidth = imageWidth;
		else
			this.imageWidth = 0;

	}

	public int getImageWidth() {//Returns width private instance variable
		return imageWidth;

	}

	public void setImageLength(int imageLength){//Receives length value and assigns
		if (imageLength > 0)
			this.imageLength = imageLength;
		else
			this.imageLength = 0;

	}

	public int getImageLength() {//Returns length private instance variable
		return imageLength;

	}

	public void setImageHint(int imageHint) {//Receives hint value and assigns
		if (imageHint > 0)
			this.imageHint = imageHint;
		else
			this.imageHint = 0;

	}

	public int getImageHint() {//Returns hint private instance variable
		return imageHint;
	}
}//End of Image class
