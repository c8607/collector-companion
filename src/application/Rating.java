package application;


public class  Rating {

	//Attributes
	private Boolean ratedTitle;

	//Constructors
	public Rating() {
		ratedTitle = false;
	}

	public Rating(Boolean ratedTitle){
		this.setRatedTitle(ratedTitle);
	}

	//Setters and Getters
	public void setRatedTitle(Boolean ratedTitle) {//Receives input from check-box status and assigns off/on
		if (ratedTitle = true)
			this.ratedTitle = true;
		else
			this.ratedTitle = false;
	}

	public Boolean getRatedTitle() { //Returns rating status
		return ratedTitle;
	}

}//End of Rating class
