package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

//Testing database connection from eclipse to MySQL workbench
public class DataIO {

	public DataIO(){

		try {
			//Constants
		    final String DATABASE_NAME = "collector_companion";
		    final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/" + DATABASE_NAME;
		    final String USER_NAME = "";
		    final String PASSWORD = "";
		//Connection to database

		//Driver
		Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER_NAME, PASSWORD);
		//Statement
		Statement s = conn.createStatement();
		System.out.println("Connection established successfully.");

		ResultSet rs = s.executeQuery("SELECT * FROM user");
		while(rs.next())
		{
			//This query pulls the user signature
			System.out.println(rs.getString(2));
		}
        //Close database connection
        conn.close();
        
		//ResultSet
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Could not connect to database");

		}
	}
}
