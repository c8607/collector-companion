package application;


public class Game {

	//Attributes
	private String gameTitle;

	//Constructors
	public Game() {
		gameTitle = "N/A";
	}

	public Game(String gameTitle) {
		this.setGameTitle(gameTitle);
	}

	//Setters and Getters
	public void setGameTitle(String gameTitle) {//Receives game title and assign it
		if (gameTitle.length() > 0)
			this.gameTitle = gameTitle;
		else
			gameTitle = "Game Title Unknown";
	}

	public String getGameTitle() {//Returns game title
		return gameTitle;
	}
}//end of Game class
