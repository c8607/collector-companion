# Identify Use Cases

**Use case format:**  user role |  action verb | object

*� User selects current collection.*

*� User adds game title.*

*� User adds category.*

*� User adds sub-category.*

*� User removes category.*

*� User removes sub-category.*

*� User adds image.*

*� User removes image.*

*� User adds text.*

*� User removes text.*

*� User saves collection.*

*� User edits collection.*

*� User deletes collection.*

*� User publishes collection.*

*� User searches current collection.*

*� User selects online community collections.*

*� User searches community collections.*

*� User chooses one community collection.*

*� User rates one community collection.*

*� User selects checkbox.*
