# **Error Documentation**

# **Issues**

The following issues were faced in the Collector Companion project during the implementation phase. Issues encountered are from Eclipse and source code. The links are given to proceed to the solution information.

- [Module javafx.controls not found](#module-javafx-controls-not-found)
- [java.lang.NullPointerException: Location is required.](#location-is-required)
 

# **Solution Information**
# Module javafx controls not found

- [x] Solved: "Error occurred during initialization of boot layer java.lang.module.FindException: Module javafx.controls not found" by checking and removing unused dependencies in run configurations.

Changes were made to implement the JavaFX SDK library and arguments in the ‘Run Configurations’ of the application. Proceeding these changes, the error occurred when the application was executed. The modulepath & classpath configurations were incorrect and did not allow the JavaFX SDK to be implemented when the application was executed. The image below shows the steps taken to resolve the issue; removing the 'Mysql connector' under the 'Dependencies' Modulepath Entries.
		
![JavaFXcontrol Error](javafxcontrolserror.png)

# Location Is Required

- [x] Solved: "java.lang.NullPointerException: Location is required.
	at javafx.fxml@18.0.1/javafx.fxml.FXMLLoader.loadImpl(FXMLLoader.java:3324)
	...
	...
	...
	..." by fixing source location & removing and adding the JRE System Library and JavaFX SDK libraries.
	
The error occurred at line 14 on the 'CollectorCompanionGUI.java' file, "Parent root = FXMLLoader.load(getClass().getResource("/application/CollectorCompanionGUIScene.fxml"));" The direct location of the .fxml file was needed to build during the application run-time and the JRE System Library and JavaFX SDK libraries needed to be removed and added in to the Modulepath and Classpath configurations. The image below shows how the error was fixed.
	
![Location not found error](locationnotfound.png)