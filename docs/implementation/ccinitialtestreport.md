# Initial Test Report

## Introduction

The test report document is created following the IEEE Standard 829 template. The test reports provide information based on the testing procedures from the [Test Plan Document](./docs/implementation/cctestcases.md) and [Standard Test Plan](./docs/ccstandardtestplan.md). The following reports are present in this document:

- [Test Report #1 - Add Category](#test-report-1a)
- [Test Report #2 - Remove Category](#test-report-2a)
- [Test Report #3 - Publish a Collection](#test-report-3a)
- [Test Report #4 - Search Community Collection](#test-report-4a)
- [Test Report #5 - Select a Single Community Collection](#test-report-5a)
- [Test Report #6 - View Previously Completed Item from My Collections](#test-report-6a)
- [Test Report #7 - Add Comments](#test-report-7a)
- [Test Report #8 - Delete a Collection](#test-report-8a)
- [Test Report #9 - Save to Local Collections ](#test-report-9a)
- [Test Report #10 - Select Yes or No Prompts from Save Feature](#test-report-10a)
- [Test Report #11 - Add Community Post to My Collections](#test-report-11a)
- [Test Report #12 - Update Comments](#test-report-12a)
- [Test Report #13 - Update text](#test-report-13a)
- [Test Report #14 - Add User Signature](#test-report-14a)




## Test Report 1A

|  Add a Category 			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |
| **Test Identifier(ID):** 		 15 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  
  


## Test Report 2A

|  Remove Category			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |
| **Test Identifier(ID):** 		 15 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  
 



## Test Report 3A

|   Publish a Collection			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |
| **Test Identifier(ID):** 		 14  |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  



## Test Report 4A

|   Search Community Collections			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |
| **Test Identifier(ID):** 		 03 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  



## Test Report 5A

|   Select a Single Community Collection			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 | 
| **Test Identifier(ID):** 		 18 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  
 


## Test Report 6A

|   View Previously Completed Item from My Collections			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |
| **Test Identifier(ID):** 		 4 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  


## Test Report 7A

| Add Comments  			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |  
| **Test Identifier(ID):** 		 7 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  


## Test Report 8A

|   Delete a Collection			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |  
| **Test Identifier(ID):** 		 5 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  



## Test Report 9A

|  Save to Local Collections			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 | 
| **Test Identifier(ID):** 		 13 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  



## Test Report 10A

|   Select Yes or No from Save Feature Prompt 	|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |  
| **Test Identifier(ID):** 		 17 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  



## Test Report 11A

|   Add Community Post to My Collections			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |
| **Test Identifier(ID):** 		 19 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  



## Test Report 12A

|  Update Comments			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 |  
| **Test Identifier(ID):** 		 11 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  


## Test Report 13A

|   Update Text			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 | 
| **Test Identifier(ID):** 		 10 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  


## Test Report 14A

|   Add User Signature			|  		
| ------------- | 	
| **Test Report ID:** 		 0551 | 
| **Test Identifier(ID):** 		 08 |  
| **Summary:** 	Results pending... |  
| **Assessment:**  TBD. |   
| <p>**Incident Description**<p/>  <p>Inputs: </p> <p>Expected Results: </p> <p>Actual Results: </p> <p>Unexpected Anomalies during testing: </p> <p>Defect from test case step: </p> <p>Additional Info of Defect: </p>   |  
| **Impact:** Impact description...	 <p>Severity: </p> <p>Priority: </p>   |  



