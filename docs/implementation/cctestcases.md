# Test Cases

## Introduction

The test cases document is created following the IEEE Standard 829 template. The test case document provides test case information based on the classes in the Collector Companion system. The following test cases are present in this document:

- [Test Case #1 - Add Category](#test-case-1a)
- [Test Case #2 - Remove Category](#test-case-2a)
- [Test Case #3 - Publish a Collection](#test-case-3a)
- [Test Case #4 - Search Community Collection](#test-case-4a)
- [Test Case #5 - Select a Single Community Collection](#test-case-5a)
- [Test Case #6 - View Previously Completed Item from My Collections](#test-case-6a)
- [Test Case #7 - Add Comments](#test-case-7a)
- [Test Case #8 - Delete a Collection](#test-case-8a)
- [Test Case #9 - Save to Local Collections ](#test-case-9a)
- [Test Case #10 - Select Yes or No Prompts from Save Feature](#test-case-10a)
- [Test Case #11 - Add Community Post to My Collections](#test-case-11a)
- [Test Case #12 - Update Comments](#test-case-12a)
- [Test Case #13 - Update text](#test-case-13a)
- [Test Case #14 - Add User Signature](#test-case-14a)




## Test Case 1A

|  Add a Category 			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 15 |  
| **Requirements Addressed:** 	 Requirement 15, section Add or Drop Categories is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'My Collections" section, and have selected the '+' Graphical User Interface (GUI) component, and be in an active collection window. |  
| **Test Input:** 	Inputs include clicking 'My Collections', clicking '+' GUI component, entering a main category title, and clicking '+' symbol on main category box.   |
| **Expected Test Results:** 	 After clicking the '+' GUI component in an active collection window, an category box is shown on the screen. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when a category component has appeared after clicking the '+' symbol on a category box in an active collection window. Results failed when a category component has not appeared after clicking the '+' symbol in an active collection window.  |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 Select '+' GUI component in list of My Collections.</p> <p>#3 Add game title to top category box.</p> <p>#4 Select '+' symbol above game category box.</p>  |  
| **Features to Be Tested:** 	 The features that are involved in this test are the graphical user interface allowing the user to add a category to a collection. |  


## Test Case 2A

|  Remove Category			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD)|	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 15 |  
| **Requirements Addressed:** 	 Requirement 15, section Add or Drop Categories is included in this test case.  |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'My Collections" section, and have selected the '+' Graphical User Interface (GUI) component, and be in an active collection window. |  
| **Test Input:** 				Inputs include clicking 'My Collections', clicking '+' GUI component, entering a main category title, clicking '+' symbol above main category box, and clicking '-' symbol on current category box.    |  
| **Expected Test Results:** 	 After clicking the '-' GUI component, the category box is removed. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when a category component has disappeared after clicking the '-' symbol on a category box in an active collection window. Results failed when a category component did not disappeared after clicking the '-' symbol in an active collection window. |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections'</p> <p>#2 Select '+' GUI component in list of My Collections.</p> <p>#3 Add game title to top category box.</p> <p>#4 Select '+' symbol above game category box.</p> <p>#5 Select '-' symbol from current category box.</p>  |  
| **Features to Be Tested:** 	 The features that that are involved in this test are the graphical user interface allowing the user to remove a category from the collection.|  



## Test Case 3A

|   Publish a Collection			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 14  |  
| **Requirements Addressed:** 	 Requirement 14, section Cloud Save Option is included in this test case.  |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'My Collections" section, and have selected the '+' Graphical User Interface (GUI) component, be in an active collection window, and have added several components including a signature name.  |  
| **Test Input:** 			Inputs include clicking 'My Collections', clicking '+' GUI component, entering a main category title, clicking '+' symbol above main category box, adding the sub-category name, selecting the '+' symbol on the current category box, add a signature name, and selecting publish and yes buttons. |  
| **Expected Test Results:** 	 The published collection will be saved to �My Collection� and �Community Collections� and appear in both places.  |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the collection has saved, including all components that were added, and appear in both local and public collections. Results have failed if the collection has not saved, is missing components, and does not appear in either local or public collections.  |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 Select '+' GUI component in list of My Collections.</p> <p>#3 Add game title to top category box.</p> <p>#4 Select '+' symbol above game category box.</p> <p>#5 Add Category name by clicking in the category text box.</p><p>#6 Select '+' symbol on the right from current category box.</p> <p>#7 Add sub-category name by clicking the sub-category text box.</p> <p>#8 Add signature name at the bottom of the screen.<p/> <p>#9 Select 'Publish' GUI component.</p> <p>#10 Select 'Yes' from prompt.</p> |  
| **Features to Be Tested:** 	 The features involved in this test is the user being able to navigate and add information using the graphical user interface to publish a Collection to the cloud. |  


## Test Case 4A

|   Search Community Collections			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 03 |  
| **Requirements Addressed:** 	 Requirement 03, section Search from Collections is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'Community Collections" section.  |  
| **Test Input:** 				Inputs include clicking 'Community Collections', clicking on the search bar, and entering information into the search bar. |  
| **Expected Test Results:** 	 When the search bar is used, the community collections filter accordingly. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the user types in a game title and the results filter automatically and display the correct results. Results have failed if the text box does not allow the user to search, the entered title does not match the results displayed on the screen, or after entering the title the results do not filter. |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'Community Collections'.</p> <p>#2 Place mouse cursor over search bar and click.</p> <p>#3 Enter a game title.</p> |  
| **Features to Be Tested:** 	 The features involved in test 4A is the user is able to search through the community collections. |  


## Test Case 5A

|   Select a Single Community Collection			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 18 |  
| **Requirements Addressed:** 	 Requirement 02 & 18, sections Select from Home GUI and Selecting Community Collections is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'Community Collections" section, and the application should show all community collections.|  
| **Test Input:** 				 Inputs include clicking 'Community Collections', clicking on the search bar, and clicking into a community collection. |  
| **Expected Test Results:** 	 When the community collection is double clicked, it opens, and displays on the screen. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the user double clicks a collection and the application displays the item and displays all information correctly. Results have failed  when the user double clicks a collection and the application does not proceed to display the item or displays the item with missing information.  |  
|<p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'Community Collections' on the main menu.</p> <p>#2 Place mouse cursor over a title that is listed and click to proceed into it.</p> |  
| **Features to Be Tested:** 	 The features involved in test 5A is the selecting a single community post by navigating the graphical user interface. |  


## Test Case 6A

|   View Previously Completed Item from My Collections			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 4 |  
| **Requirements Addressed:** 	 Requirement 01 & 4, sections Application Startup/GUI and UI Shows List of Collections is included in this test case. |
| **Prerequisite Conditions:**	 The end-user should have selected the 'My Collections" section, and the application should show all current locally saved collections. |  
| **Test Input:** 				Inputs include clicking 'My Collections' and selecting the collection that was edited.  |  
| **Expected Test Results:** 	 Collection that was previously saved to 'My Collections' is still available with all previously added information. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when selecting the edited collection and being able to view all the information that was saved. Results have failed when collection is not present in local collections or collection is present, selected, and does not display correct information when it was saved. |  
| <p>**Instructions for Conducting Procedure:**<p/>  <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 Verify that collection previously created is displayed.</p> <p>#3 Select that collection and view if correct elements were added and displayed.</p> |  
| **Features to Be Tested:** 	 Features involved in test 6A is using the graphical user interface to select from collections and view all collections in a list form.  |  


## Test Case 7A

| Add Comments  			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 7 |  
| **Requirements Addressed:** 	 Requirement 7, section Add Comments is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'My Collections" section, and have selected the '+' Graphical User Interface (GUI) component, and be in an active collection window. |  
| **Test Input:** 				Inputs include clicking 'My Collections' and selecting the '+' GUI component, inserting a game title, selecting the '+' symbol above the category box, inserting a category name, selecting a 'C' button, inserting comments into text box, inserting a user signature, clicking save and yes button, and selecting 'My Collections' and recent collection. |  
| **Expected Test Results:** 	 'C' GUI component allows for comments to be placed in comment box, and when the collection is saved they are still available and visible. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the end-user is able to click the 'C' component on the category, is able to insert comments, and the comments appear after being saved. Results have failed when 'C' component does trigger and display comment feature, if the comment box does not allow text, or if comments are not on saved collection.  |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 Select '+' GUI component in list of My Collections.</p> <p>#3 Add game title to top category box.</p> <p>#4 Select '+' symbol above game category box.</p> <p> #5 Add category name to current category box. </p> <p>#6 Select 'C' GUI component and type in text box any comment information.<p/> <p>#7 Add user signature in text box at bottom of screen. <p/> <p>#8 Click 'Save' GUI component.</p> <p>#9 Select 'Yes' from prompt.</p> <p>#10 Return to main menu<p/> <p>#11 Select 'My Collections' from the main menu.</p> <p> #12 Select most recent collection created<p/> <p> #13 Verify if comments are present.<p/>  |  
| **Features to Be Tested:** 	 Features involved in test 7A is using the GUI component to add comments to category and sub-category items.|  


## Test Case 8A

|   Delete a Collection			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 5 |  
| **Requirements Addressed:** 	 Requirement 5, section Editing Options is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'My Collections" section, selected the one collection that is displayed, and be in an active collection window. |  
| **Test Input:** 				Inputs include clicking 'My Collections' and selecting the collection, clicking the 'Delete' graphical user interface (GUI) component, and selecting options on confirmation window.|  
| **Expected Test Results:** 	 Collection that is deleted is no longer available or visible in "My Collections" list. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the collection is no longer available in local collections. Results have failed when delete component does not trigger confirmation prompt, or if collection is still available in local collections after going through deleting steps.  |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 Select any item listed in My Collections.</p> <p>#3 Click 'Delete' GUI component in the active window.<p/> <p>#4 Return to main menu<p/> <p>#5 Select 'My Collections'.<p/> <p>#6 Verify if deleted collection is visible in the list of collections shown in active window.<p/> |  
| **Features to Be Tested:** 	Features involved in test 8A is the application allowing the user to delete a collection. |  


## Test Case 9A

|  Save to Local Collections			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 13 |  
| **Requirements Addressed:** 	 Requirement 13, section Local Save Option is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected 'My Collections" , have selected the '+' GUI component, be in an active collection window, and have several components added in the collection. |  
| **Test Input:** 				 Inputs include clicking 'My Collections', clicking '+' GUI component, entering a main category title, clicking '+' symbol above main category box, adding the sub-category name, selecting the '+' symbol on the right of the current category box, and selecting save and yes buttons. |  
| **Expected Test Results:** 	 Collection that end-user prepared is saved and is present in local "My Collections". |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the user successfully saves when through the save prompt and the collection appears in the local collection section, 'My Collections'. Results have failed if the save prompt does not appear or the collection is not present in 'My Collections'.   |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 Select '+' GUI component in list of My Collections.</p> <p>#3 Add game title to top category box.</p> <p>#4 Select '+' symbol above game category box.</p> <p>#5 Add Category name by clicking in the category text box.</p><p>#6 Select '+' symbol on the right from current category box.</p> <p>#7 Add sub-category name by clicking the sub-category text box.</p> <p>#8 Add user signature to the bottom corner text box. <p/> <p>#9 Select 'Save' GUI component.</p> <p>#10 Select 'Yes' from prompt.</p> <p> #11 Return to main menu.<p/> <p>#12 Select 'My Collections'.<p/> <p> #13 Verify that collection is listed in locally saved collections. <p/> |  
| **Features to Be Tested:** 	 Features involved in test 9A is the application allows the user to save the collection to local collections called 'My Collections'. |  


## Test Case 10A

|   Select Yes or No from Save Feature Prompt 	|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 17 |  
| **Requirements Addressed:** 	 Requirement 09 & 17, section UI Post Preview & UI Alerts is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'My Collections" section, selected the '+' GUI component, be in an active collection window, have several components added in the collection, and clicked the 'Save' component. |  
| **Test Input:** 				  Inputs include clicking 'My Collections', clicking '+' GUI component, entering a main category title, clicking '+' symbol above main category box, adding the sub-category name, selecting the '+' symbol on the right of the current category box, inserting a name into the category box, and selecting save and yes buttons.  |  
| **Expected Test Results:** 	When 'Yes' button is clicked, the collection is saved to My Collections, and if 'No' button is clicked it should return end-user to collection that they are working on.  |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the prompt save feature appears, allows the user to click buttons, and both buttons proceed to either save with pressing 'Yes' or returns to collection when pressing 'No'. Results have failed if the prompt does not appear, or if the buttons do not work, or proceed to save or return to the collection. |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 Select '+' GUI component in list of My Collections.</p> <p>#3 Add game title to top category box.</p> <p>#4 Select '+' symbol above game category box.</p> <p>#5 Add Category name by clicking in the category text box.</p><p>#6 Select '+' symbol on the right from current category box.</p> <p>#7 Add sub-category name by clicking the sub-category text box.</p> <p>#8 Add a user signature at the bottom right of the screen.<p/> <p>#9 Select 'Save' GUI component.</p> <p>#10 Select 'Yes' from prompt.</p> </p> #11 Proceed to main menu.</p> |  
| **Features to Be Tested:** 	Features involved in test 10A is the application shows the added components and alerts the user before saving the collection.|  


## Test Case 11A

|   Add Community Post to My Collections			|  		
| ------------- | 	
| **Test Case Creator:** 		Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 19 |  
| **Requirements Addressed:** 	 Requirement 19, section Save Community Collection is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user selected the 'Community Collections" section and selected 'Add' GUI component.  |  
| **Test Input:** 				Inputs include clicking 'Community Collections', clicking into a community collection, clicking save and yes buttons.  |
| **Expected Test Results:** 	 Community post should be present in locally saved collections under "My Collections". |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the community post is shown and saved in 'My Collection'. Results have failed if the add GUI component does not proceed to save the collection or if the saved collection is not shown or saved in 'My Collection'. |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'Community Collections' on the main menu.</p> <p>#2 Place mouse cursor over a title that is listed and click to proceed into it.</p> <p>#3 Select 'Add' in active community collection window.<p/> <p>#4 Select 'Yes' on prompt window.<p/> <p>#5 Return to main menu<p/> <p>#6 Select 'My Collection' in main menu.<p/> <p>#7 Verify if community collection saved is in 'My Collection' list.<p/> |  
| **Features to Be Tested:** 	Features involved in test 11A is the application allows the user to save a community collection to their local collection. |  


## Test Case 12A

|  Update Comments			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 11 |  
| **Requirements Addressed:** 	 Requirement 11, section Update Comments is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected 'My Collections" section, selected one collection, and be in an active collection window.|  
| **Test Input:** 				 Inputs include clicking 'My Collections', selecting the collection that was edited, clicking on a category with comments, and inserting new information into category or sub-category comment boxes. |  
| **Expected Test Results:** 	 New comments should appear when viewing the updated local collection. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when comments are updated and seen in the collection when it is closed and viewed again. Results have failed when updated comments do not appear when collection is viewed. |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 View collections that are displayed.</p> <p>#3 Select one collection.</p> <p>#4 Double click a category box that has comments added.<p/> <p>#5 Add more comments or alter comments.<p/> <p>#6 Click 'Save' GUI component.<p/> <p>#7 Click 'Yes' on prompt.<p/> <p>#8 Return to main menu.<p/> <p>#9 Select 'My Collections' from the main menu.</p> <p>#10 Click on the collection that was edited. </p> <p>#11 Verify if comments are updated.<p/> |  
| **Features to Be Tested:** 	Features involved in test 12A is the application allowing the user to update category and sub-category comments using text boxes.|  


## Test Case 13A

|   Update Text			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 10 |  
| **Requirements Addressed:** 	 Requirement 10, section Update Text Options is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected the 'My Collections" section, selected one collection, and be in an active collection window |  
| **Test Input:** 				Inputs include clicking 'My Collections', selecting the collection that was edited, clicking on a category name, and inserting information into category name. |  
| **Expected Test Results:** 	 New text should appear when viewed in a locally saved collection. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when category text box can be double clicked to edit and after changed and saved it appears when reviewing the collection. Results have failed when text box does not allow text to be inserted, or if text information does not appear after collection is reviewed.  |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 View collections that are displayed.</p> <p>#3 Select one collection.</p> <p>#4 Double click a category box to change the text.<p/> <p>#5 Add a different category name.<p/> <p>#6 Click 'Save' GUI component.<p/> <p>#7 Click 'Yes' on prompt.<p/> <p>#8 Return to main menu.<p/> <p>#9 Select 'My Collections' from the main menu.</p> <p>#10 View collections that are displayed for updated collection.</p> <p>#11 Select collection that was edited.</p> <p>#12 Verify if category name text box is changed based on previous input.<p/> |  
| **Features to Be Tested:** 	 Features involved in test 13A is the application allowing the user to update categories and sub-category text boxes.|  


## Test Case 14A

|   Add User Signature			|  		
| ------------- | 	
| **Test Case Creator:** 		 Kimberly Hernandez |
| **Tester Name:**				 To be determined (TBD) |	 
| **Test Date:** 				 TBD |  
| **Test Identifier(ID):** 		 08 |  
| **Requirements Addressed:** 	 Requirement 08, section Apply Signature Name is included in this test case. |  
| **Prerequisite Conditions:**	 The end-user should have selected 'My Collections" section, selected '+' GUI component to add a new collection, and be in an active collection window |  
| **Test Input:** 				 Inputs include clicking 'My Collections', clicking '+' GUI component, entering a main category title, clicking '+' symbol above main category box, adding the sub-category name, selecting the '+' symbol on the right of the current sub-category box, inserting information into category box and user signature, and selecting save and yes buttons. |  
| **Expected Test Results:** 	 Signature should be present when viewed in the locally saved collections. |  
| **Criteria for Evaluating Results:** 	Results have succeeded when the text box allows the user to enter a signature and when saved is present on screen when collection is reviewed. Results have failed when signature text box does not allow information to be inserted or if signature does not appear on the screen when collection is reviewed.   |  
| <p>**Instructions for Conducting Procedure:**<p/> <p>#1 Select 'My Collections' from the main menu.</p> <p>#2 Select '+' GUI component in list of My Collections.</p> <p>#3 Add game title to top category box.</p> <p>#4 Select '+' symbol above game category box.</p> <p>#5 Add a category name.<p/>  <p>#6 Add signature name in text-box on bottom right corner.<p/> <p>#7 Click 'Save' GUI component. <p/> <p>#8 Click 'Yes' on prompt.<p/> <p>#9 Return to main menu.<p/> <p>#10 Select 'My Collections' from the main menu.</p> <p>#11 View collections that are displayed for currently updated collection.</p> <p>#12 Select collection that was edited.</p> <p>#13 Verify if signature name is present in collection.<p/> |  
| **Features to Be Tested:** 	Features involved in test 14A is the application allowing the user to apply a signature name to the end of the post.|  


