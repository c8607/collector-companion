# Project Proposal


## Overview

## Project Background and Description

In todays gaming world challenges continue for developers to bring their communities more features to enjoy. Players continue to expect these exciting features to bring more goals, challenges, or even areas to explore in their favorite games. To keep the gamer close to the community and many features of their favorite games, Collector Companion is an application idea that aims to do so. The idea sparked from playing a variety of games and returning to them because of their replayability, but there is a disconnect between the communities that explore and share what they have found in their game playthrough. These games have a vast number of features allowing a percentage of the gaming community to break down the game and show what was found in the game through social media. But social media continues to constantly update losing this information, and with guidebooks being off the shelves for years now, this application will allow the end-user to have their Collector Companion available to them at any time to truly reconnect them to their game and community. Collector Companion will allow gamers to continue their goals, challenges, and collection of in-game items all in one place. The application will bring flexibility to the end-user. End-users will be able to track multiple favorite games, add categories and subcategories, add images for visual pointers, and comments for additional information or export to other sources. The end-user will be able to save or delete their game collections for the rest of the community to explore. 


## Project Scope

As stated previously Collector Companion will provide flexibility to the end-user. This includes full customization based on the users choice of game they are working on. The application will have a menu that shows the user their current collection of games they are working on or shows a search section for community posts. When the user selects current games to work on it will show a list of game titles. When selecting a title, the user can update the features the application has to offer such as titles, comments, checkboxes, and images for visual aids. Sub-texts will be allowed for sub-categories of items that are listed. The user can save or delete what they have worked on or publish it, so the community has access to it. Saving, editing, deleting, or publishing will show the user what it looks like before proceeding to do these options. Game title, categories with a drop-down to show sub-categories of items added, sub-categories of items with checkboxes, comments on sub-category items added, specific game-related images, and the signature of the user who created it will display on this screen. A search bar will be provided on each post allowing all users to quickly find keywords of items or information on the game. When the user selects to search from the main menu, a game title or signature name of the user filters results and will display them. Multiple users will have the same game posted leaving all users the ability to rate by liking another users post. This will allow the search to display the most liked post at the top. The user will need access to the internet to retrieve information from the search feature otherwise will have access to their current inventory of games. Information will be saved to MySQL workbench during development. When placed into production for online community viewing or user publishing, the application's data will save to a cloud database. This application will be developed first for Windows PC users. Users will be able to download the application through the GitHub Marketplace. This application will not be available to Android and iOS devices yet.


## High-level Requirements

Collector Companion must include the following:

 Provide flexibility to the end-user. 

 Ability to fully customize based on the users choice of game. 

 A menu option for users current collection of games they are working on.

 A search menu option for the collection of community posts. 

 Display users' current collection in list form by game title name. 

 Ability to save, edit, delete, or publish.

 Ability to add images for sub-category item visual aids. 

 Ability to add comments to sub-category items. 

 Allow the end-user to apply a signature name. 

 Ability to update titles, categories, and sub-categories text, comments, checkboxes, images, and user signature name features.

 Ability to view current work before proceeding to edit, save, delete, or publish options.

 Allow users to save locally or save on the cloud for community viewing.

 Provided a drop-down menu to show sub-items listed in main categories. 

 Sub-categories of items provided a check box.

 Allow the user to use a search bar for game information or keywords.

 Searches filter down by game or user signature name with the most likes at the top.

 Allow all users to rate posts by clicking on a like button.

 Internet access allows other users to search from published posts.

 Ability to download on Windows PC through GitHub Marketplace. 

## Deliverables

For Start-up Software to deliver the Collector Companion application for the gaming community, the following deliverables must be completed:

 <ins>Requirements gathering</ins> including project scope and high-level requirements must be gathered based on end-user and client input. 

 <ins>Planning</ins> starts with concluding the requirements and then creating the UML-OOAD diagrams.

 <ins>Analysis</ins> must be completed for software architecture and decisions for the designing phase.

 <ins>Designing</ins> the application must take place before software construction.

 <ins>Implementation</ins> must take place in the following order software construction, testing, and maintenance before the release of the application.

## Affected Parties

 Gaming end-user/clients - may or may not fit into their gaming lifestyle; easier or harder to enjoy the game they plan on using the application for.

 Project developers - application requirements and objectives based on stakeholders input may be unrealistic for development.

 Database department  new application may not be suitable for the current database environment. 

 Quality assurance staff - chance that the application is not suitable for current PC working environments.

## Affected Business Processes or Systems

Since this is a new application that will be developed as a student project there will be no businesses or systems that will be impacted.

## Specific Exclusions from Scope

Components excluded from this project:

 The application will not allow users to export posted information.

 Videos will not replace the visual aid feature.

 Other PC systems that are not Windows OS will not be able to run the application.

 Users will not be able to chat.

 Users will not be able to add other users.

## Implementation Plan

The Collector Companion will be implemented by using Eclipse IDE, and MySQL Workbench, and passed to the end-user through GitHub when rolled out for release. The traditional software development life cycle will be used to take the project through the phases of development. To meet the goals of the project major deliverables will be broken down and implemented based on the timeline/schedule (see the section below). Each phase must be completed before the next to ensure the application is rolled out to GitHub by 6/25/2022. 

## High-level Timeline/Schedule

 <ins>Requirements gathering</ins>  Project scope & High-Level Requirements estimated completion time 5/8/2022

 <ins>Planning</ins> - Requirements & UML-OOAD Diagrams estimated completion time: 5/15/2022

 <ins>Analysis</ins>  Software Architecture estimated completion time: 5/29/2022

 <ins>Designing</ins>  Software Designing estimated completion time: 6/1/2022

 <ins>Implementation</ins>  Software Construction, Testing, Maintenance, & Release of Application estimated completion time: 6/25/2022

## Approval and Authority to Proceed

|  Name: 			| Title:				   |  Date: 		|
| ------------- 	| ------------- 		   | ------------- 	|	
| Kimberly Hernandez| DeVry University Student | 5/5/2022 		| 
| 	 				|						   | 				|
| 	 				|						   | 				|



