# Software Architecture Description

The Model-View-Controller (MVC), Object-Oriented (OO), and Data-Centered (DC) architectures are used for the Collector Companion project. These architectures align with the quality attributes given in the Software Requirements Specifications (SRS). The quality attributes listed in the SRS are *[availability](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#availability)*, *[functional suitability](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#functional-suitability)*, *[maintainability](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#maintainability)*, *[reusability](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#reusability)*, and *[usability](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#usability)*. The architectures are described below and how they support the project's quality attributes.

## Model-View-Controller Architecture

The view component of the MVC architecture supports the *usability* attribute for providing user information requests in a graphical format. The *usability* attribute is vital for easy application navigation. The control component connects the view and model components by handling user processes and supports the *functional suitability* attribute. End-users will access the data through the view and with the controller components. The Model component focuses on the system's data and supports the project's *maintainability*, *reusability*, and *functional suitability* attributes. The MVC architecture's control and model components will be combined with the OO and DC architectures to benefit from them and improve the overall software structure. 

![ccmvc](ccmvc.png)

## Object-Oriented Architecture
 
The OO architecture represents the end-user and internal processes in the Collector Companion application. These  processes are broken down into classes and methods to extend throughout and maintain the system. The OO architecture supports the *maintainability*, *reusability*, *functional suitability*, and *availability* attributes. In addition, the OO architecture saves time for developers with code reusability. 

![ccooa](ccooa.png)

## Data-Centered Architecture

The DC architecture represents data being stored and accessed by different end-users. The database system communicates with the central system (or the controller component in the MVC) in managing the application's data. The DC architecture supports the *maintainability*, *functional suitability*, and *availability* attributes.

![ccdca](ccdca.png)

# Static View

The static view is a UML component diagram showing the application's structure with the components and connections. The static view combines the Model-View-Controller (MVC), Object-Oriented (OO), and Data-Centered (DC) architectures. The architecture's primary focus is the MVC, where the controller displays the OO, and the model shows the DC. The view and controller are connected to send data or perform requests to the model, and the view is updated when the data is returned.

![Static View](ccstaticviewA.png)


# Dynamic View

The dynamic view is a sequence diagram of the architecture's behavior, including the components and processes. The end-user interacts with the view or graphical user interface (GUI). Components are triggered by activating the controller or application�s addCollectionEvent() or searchButtonEvent() methods. The model or database manages the controller's information and is returned to the user on the GUI.  

![Dynamic View](ccdynamicview.png)