# Software Design Description

## Revisions
|  Date: 		| Revision: 	|  Description: 				|  Author: 			|
| ------------- | ------------- | ------------- 				| ------------- 	|
| <6/4/2022> 	| 1.0  			|  Added component designs  	| Kimberly Hernandez  |
| <6/5/2022> 	| 1.1  			|  Added document information   |  Kimberly Hernandez   |
| <6/6/2022> 	| 1.2  			|  Added document information & edited diagrams   |  Kimberly Hernandez   |
| <6/10/2022> 	| 1.2  			|  Added ERD diagram   |  Kimberly Hernandez   |
| <6/11/2022> 	| 1.3  			|  Updated Human Interface Design  |  Kimberly Hernandez   |
| <6/12/2022> 	| 1.4  			|  Added tests in requirements matrix   |  Kimberly Hernandez   |

## Introduction

Collector Companion will allow gamers to continue their goals, challenges, and collection of in-game items all in one place. The application will bring flexibility to the end-user with a variety of editing features. End-users will be able to track multiple favorite games, add categories and sub-categories, add images for visual pointers, and comments for additional information or export to other sources. The end-user will be able to save their game collections for the rest of the community to explore.

The software design description (SDD) provides information on the Collector Companion system for code construction. This SDD was prepared by referencing the IEEE Standard 1016 SDD template. All information provided was based on the
[software requirements specification](./docs/ccsrs.md), 
[use case descriptions](./docs/ccbasicusecasedescriptions.md), 
[class diagrams](./docs/cc1classdiagram.pdf), 
[view of participating classes matrix](./docs/cc1vopcmatrix.pdf), and [software architectures description](./docs/design/SoftwareArchitectureDescription.md) created previously. This document is intended for the software developers and other stakeholders of the project. This SDD includes the system overview, architecture, data dictionary, component designs, human interface design, and requirements matrix for the Collector Companion project.


## System Overview

The Collector Companion system will allow end-users to keep track of various in-game features by editing game information to build a post and add the post to their overall collection and public viewing. Key features include searching through local or public collections and creating a post by adding the game title, category, and sub-categories that allow comments, ratings, images, and a user signature. The primary user activity will be editing game information, and the subsystems involved are the graphical user interface where editing options are accessible and where navigation will occur. Additional functionality of the system is database interaction which the system architecture and component design capture the visualization of the exchange below.  

## System Architecture

The system's architecture was based on supporting the availability, functional suitability, maintainability, reusability, and usability quality attributes. The architecture consists of the model-view-controller (MVC), Object-Oriented (OO), and Data-Centered (DC) architectures. The MVC represents user information requests in a graphical format, handling user processes and the system's data. The OO represents the controller of the MVC internal processes done by the components. The DC represents the model of the MVC where the database system communicates with the central system. The three architectures were combined to create a UML component diagram or static diagram and a UML sequence diagram or dynamic view of the system architecture. By having these architectures meet the non-functional requirements of the system.

## Data Dictionary

**DataIO**

DataIO is the database class where data operations are performed to manage the application data.

**CollectorCompanionGUI**

Collector Companion GUI is the main class where the application starts and implements graphical user interface methods.

**Collection Builder**

Collection Builder is a builder pattern class that builds on different elements to create a single collection.

**CreateCollection**

CreateCollection is a class that creates the collection into an array list.

**Interface**

**Collection**

Interface Collection is the interface for a collection where methods are present but are defined in the sub-classes.

**UsersGame**

UsersGame is an abstract class describing the different elements a user's game may contain and where methods are defined in the sub-classes.

**UserInfo**

UserInfo is a class for the user signature. 

**Game**

The Game class represents the game title information.

**Category**

The Category is a class that represents the categories that the post will contain.

**Subcategory**

A Subcategory represents a class for the sub-categories of a post.

**Rating**

The Rating class determines whether true or false if the post has been rated not.

**Image**

Represents a class for an image that a post can contain and adjusts the size of the picture. 

**Boolean**

Boolean is a primitive data type where the attribute or method returns true or false.

**int**

Int is a primitive data type where an attribute or method represents a numeric value.

**String**

A string is a data type that is considered primitive and represents characters in sequence.


## Component Design

<ins>Static Object-Oriented Design (OOD) View</ins>

The UML class diagram below is the static design of the system. The CollectorCompanionGUI component will allow the end-user to trigger myCollections() or searchCollections() event methods. The myCollections() method allows retrieval and build of collections. The searchCollections() method allows searching local or public inventories. The CollectionBuilder, CreateCollection, and  Interface Collection components allow the system to build a collection that contains different elements that the sub-classes will define. Different elements defined and pulled by the sub-classes are the Users-Game, UserInfo, Game, Category, Subcategory, Rating, and Image components. The DataIO component will handle all the data that the other components create. The DataIO of the system will allow for adding, viewing, deleting, editing, and searching of the system�s data, depicted in the entity relationship diagram. 

![ccoodsv](ccoodstaticview.png)

<ins>Entity Relationship Diagram(ERD):</ins>

The ERD that depicts the system's data that is managed by the DataIO component.

![ccerd](ccerd.png)


<ins>Dynamic Object-Oriented Design View</ins>

The collaboration diagram below is the dynamic design of the system. The system starts with the user's request to add a collection by pressing the My Collections GUI button and triggers the CollectionBuilder class. The CollectionBuilder executes the CreateCollection class, the CreateCollection runs the Interface Collection, and the Interface Collection class communicates with UsersGame. The UsersGame class communicates with the following: Game, Category, Subcategory, Rating, Image, and UserInfo to build an entire collection. The whole object is brought back to the CreateCollection that places it into an ArrayList. When the user saves the information, the CollectionBuilder will pass it to the DataIO component.

![ccooddv](ccooddynamicview.png)


## Human Interface Design

The example below shows a preview of the system screen. Example 1 shows that the user can select from two options: My Collections or Search Collections. When the user selects My Collections, the system will proceed to example 2. Example 2 shows if the user decides to search from their collection. The system will display all of the collections saved from the DataIO component. When the user decides to select an item from their current collection, example 5 will display. When the user selects the plus symbol on the graphical user interface (GUI), the system will execute the CollectionBuilder component and allow the user to edit the collection. Example 4 is how the user will start to modify the collection easily. The user can edit the text by clicking the larger rectangles, which are the categories, and the smaller rectangles, which are the sub-categories. The plus and minus symbols add or remove the components, and the C and I circle icons are the comments and images that can be added. The system will respond to each GUI component by the event methods assigned. The save, delete, and publish buttons will display the confirmation prompt as seen on examples 4.A, 4.B, & 4.C. When the user chooses to search the collections, the system will respond by the event method set and triggers the DataIO component. Example 3 will be displayed to the user. The user can select or search from public collections that are displayed. Example 3A shows when the collection is clicked for viewing and the user has the option to add it to their collection which will prompt for confirmation.

![ccGUI](ccgui.png)


## Requirements Matrix

Below is the matrix based on the requirements of the Collector Companion. A brief description of the system requirement is described, and the section number is the unique number given in the [SRS](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#functional-requirements) document. The functional design lists and links where the requirements are designed and referenced. The code line is where the code resides in Eclipse, and the tests column serves as the traceability matrix where the tests plan numbers are listed. Values that are not given are to be determined (TBD). System requirements that are not required to be tested are given FNTBT (or features not to be tested) values.

|  System Requirements: | Section Number: |  Functional Design:	|  Code Line: | Tests: |
| ------------- | ------------- | ------------- | ------------- | ------------- | 
| The graphical user interface shall show the current user game collection or community collection on the main screen.	| 3.2 (01) | [Interface Design](#human-interface-design) | TBD | [6A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-6a) | 
| The application shall allow the user to select the current collection or to search from community posts.| 3.2 (02)			| [Interface Design](#human-interface-design) | TBD | [5A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-5a) | 
| The application will allow the user to search local collections or community collections.	  | 3.2 (03)| [Interface Design](#human-interface-design),  [Architecture Static View](./docs/design/ccstaticviewA.png) |  TBD |  [4A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-4a)  |
| The graphical user interface shall show the user's collections in list form by game title.| 3.2 (04)| [Interface Design](#human-interface-design) |  TBD  |  [6A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-6a)  |
| The application will allow the user to save, edit, or delete their game collection. | 3.2 (05)| [Component Design](#component-design) |  TBD  |  [8A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-8a)  | 
| The application will allow the user to add images to the category or sub-category items added.| 3.2 (06)| [Component Design](#component-design) | TBD | FNTBT | 
| The application will allow the user to add comments to category and sub-category items. | 3.2 (07)| [Component Design](#component-design)  |  TBD  | [7A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-7a) |
| The application shall allow the user to apply a signature name to the end of the post.| 3.2 (08) | [Component Design](#component-design)  |  TBD  |  [14A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-14a)  |
| The user interface will show the added categories, sub-categories, comments, and images before saving, publishing, or deleting.  | 3.2 (09) | [Interface Design](#human-interface-design)  |  TBD  |  [10A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-10a)  |
| The application shall allow the user to update categories and sub-category text boxes. | 3.2 (10)| [Component Design](#component-design)  |  TBD  |  [13A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-13a)  |
| The application shall allow the user to update categories and sub-category comments using text boxes.| 3.2 (11)| [Component Design](#component-design)  |  TBD  |  [12A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-12a)  |
| The application shall allow the user to check boxes for items indicating completion.| 3.2 (12)|[Interface Design](#human-interface-design)  |  TBD  |  FNTBT  |
| The application shall allow the user to save locally for personal use.  | 3.2 (13)| [Data-Centered Architecture](./docs/design/ccdca.png), [Entity Relationship Diagram](ccerd.png)  |  TBD  |  [9A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-9a)  |
| The application shall allow the user to save on the cloud for community viewing. 	  | 3.2 (14)			| [Data-Centered Architecture](./docs/design/ccdca.png), [Entity Relationship Diagram](ccerd.png)  |  TBD  |  [3A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-3a)  |
| The application shall allow the user to add or drop categories and sub-categories by clicking the GUI buttons.	  | 3.2 (15)			| [Interface Design](#human-interface-design) |  TBD  |  [1A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-1a), [2A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-2a)  |
| The application shall allow the user to rate posts by clicking a heart button indicating it was liked otherwise no rate is given. 	  | 3.2 (16)			| [Interface Design](#human-interface-design)  |  TBD  |  FNTBT  |
| The user interface will alert the user for empty category or subcategory items.	  | 3.2 (17)			| [Component Design](#component-design)  |  TBD  |  [10A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-10a)  | 
| The application shall allow the user to select a community post.		  | 3.2 (18)			| [Interface Design](#human-interface-design)|  TBD  |  [5A](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/implementation/cctestcases.md#test-case-5a)  |


