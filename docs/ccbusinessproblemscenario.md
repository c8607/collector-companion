# **Collector Companion**


### **Revisions**
|  Date: 		| Revision: 	|  Description: 				|  Author: 			|
| ------------- | ------------- | ------------- 				| ------------- 	|
| <5/5/2022> 	| 1.0  			|  Created document  			| Kimberly Hernandez|
| <6/9/2022> 	| 1.1  			|  Transferred MS document info to MD document | Kimberly Hernandez|

### **Business Problem/Scope Statement**
In today�s gaming world challenges continue for developers to bring their communities more features to enjoy. Players continue to expect these exciting features to bring more goals, challenges, or even areas to explore in their favorite games. To keep the gamer close to the community and many features of their favorite games, Collector Companion is an application idea that aims to do so. The project was initiated by playing a variety of games and returning to them because of their replayability, but there is a disconnect between the communities that explore and share what they have found in their game playthrough. These games have a vast number of features allowing a percentage of the gaming community to break down the game and show what was found in the game through social media. But social media continues to constantly update losing this information, and with guidebooks being off the shelves for years now, this application will allow the end-user to have their Collector Companion available to them at any time to truly reconnect them to their game and community. 

Collector Companion will allow gamers to continue their goals, challenges, and collection of in-game items all in one place. The application will bring flexibility to the end-user. End-users will be able to track multiple favorite games, add categories and subcategories, add images for visual pointers, and comments for additional information or export to other sources. The end-user will be able to save or delete their game collections for the rest of the community to explore.


### **Project Objectives**
� Provide flexibility to the end-user.
 
� Ability to fully customize based on the user�s choice of game. 

� A menu option for users� current collection of games they are working on.

� A search menu option for the collection of community posts. 

� Display users' current collection in list form by game title name. 

� Ability to save, edit, delete, or publish.

� Ability to add images for subcategory item visual aids. 

� Ability to add comments to subcategory items. 

� Allow the end-user to apply a signature name. 

� Ability to update titles, categories, and subcategories text, comments, checkboxes, images, and user signature name features.

� Ability to view current work before proceeding to edit, save, delete, or publish options.

� Allow users to save locally or save on the cloud for community viewing.

� Provided a drop-down menu to show sub-items listed in main categories. 

� Sub-categories of items provided a check box.

� Allow the user to use a search bar for game information or keywords.

� Searches filter down by game or user signature name with the most likes at the top.

� Allow all users to rate posts by clicking on a like button.

� Internet access allows other users to search from published posts.

� Ability to download on Windows PC through GitHub Marketplace. 


### **Customer/Stakeholders**

�	Gaming end-user/clients - may or may not fit into their gaming lifestyle; easier or harder to enjoy the game they plan on using the application for.

�	Project developers - application requirements and objectives based on stakeholders� input may be unrealistic for development.

�	Database department � new application may not be suitable for the current database environment. 

�	Quality assurance staff - chance that the application is not suitable for current PC working environments.


### **Project Description**

<ins>Business description/high-level functional requirements:</ins>

Collector Companion will provide flexibility to the end-user. This includes full customization based on the user�s choice of game they are working on. The application will have a menu that shows the user their current collection of games they are working on or shows a search section for community posts. When the user selects current games to work on it will show a list of game titles. The user can save or delete what they have worked on or publish it, so the community has access to it. Saving, editing, deleting, or publishing will show the user what it looks like before proceeding to do these options. A search bar will be provided on each post allowing all users to quickly find keywords of items or information on the game. When the user selects to search from the main menu, a game title or signature name of the user filters results and will display them. Multiple users will have the same game posted leaving all users the ability to rate by liking another user�s post. This will allow the search to display the most liked post at the top. With Internet access, the user can download the application on GitHub as well as the search feature is available.

<ins>Technical Tools & Resources:</ins>

Resources to build the application will be on Eclipse Integrated Development Environment (IDE) for application logic (classes & methods), packages of the application (java files), and development of the graphical user interface (GUI). Oracle�s MySQL Workbench will be used to connect the application to it and save client information. Microsoft Visio will be used to design UML-OOAD Diagrams and use cases. For project management purposes the Software Requirements Specifications template will be used following the IEEE Std 830, the Project Management Body of Knowledge (PMBOK Guide) used for project management (PM) processes and practices, and Microsoft Project for the work breakdown structure (WBS). Lastly, GitHub will be used to provide clients access to the final application. 

### **Software Engineering Best Practices**

� PMBOK Guide processes and practices.

� Software development life cycle � requirements gathering, planning, analysis, design, & implementation.

� Recommended practices for Software Requirements Specifications IEEE Std 830.

� On designing the application including use cases and UML/OOAD Diagrams.

� To apply the business three-tier architecture.

### **Major Project Deliverables**

The following deliverables must be completed:

� Requirements gathering including project scope and high-level requirements must be gathered based on end-user and client input. 

� Planning starts with concluding the requirements and then creating the UML-OOAD diagrams.

� The analysis must be completed for software architecture and decisions for the designing phase.

� Designing the application must take place before software construction.

� Implementation must take place in the following order software construction, testing, and maintenance before the release of the application.

### **Individual/Team Member Responsibilities for Project Components**

All work will be done by main project member Kimberly Hernandez. All work includes project management practices and software engineering practices which contain the requirements gathering, planning, analysis, design, and implementation of the Collector Companion application. 
