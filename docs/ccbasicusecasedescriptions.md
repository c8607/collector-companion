# Use Case Descriptions


## User Adds Category
|   			|  		
| ------------- | 	
| <p>**Use Case Name:** User Adds Category</p>	 <p>**ID:** 15</p>	<p>**Importance Level:** High<p> <p>**Use Case Type:**	System </p>  |
| **Primary Actor:**	User |	 
| **Stakeholders and Interests:** Primary Actor � Gamer interested in keeping track of game information. |  
**Brief Description:** The user adds category use case defines the process of the user adding categories to the collection that is being worked on. |
| <p>**Trigger:**	Clicked GUI �+� button.</p> <p> **Type:** Button Event</p> |  
|<p>**Relationships:**</p> <p>Association: User adds category</p> <p>Include: User adds sub-category</p> <p>Extend: User adds text, User adds image</p> <p>Generalization: User creates single collection</p> | 
|<p>**Normal Flow (NF) of Events:**</p> <p> 1.	The use case starts with the user on the main menu and has two options:</p> <p>�Current Collection� or �Community Collection.</p> <p> 2.	The User clicks on �Current Collection; the S-1: Collection Screen sub-flow is performed.</p> <p> 3.	The user selects the �+� button to add a new collection; the S-2: New Collection sub-flow is performed.</p> <p> 4. The user clicks the title �+� button to add a category; S-3: Category Box sub-flow is performed.</p> | 
|<p>**Sub Flows:**</p> <p>S-1: Collection Screen</p> <p>1.	The system displays all the user�s current collection and then the use case proceeds.</p> <p>S-2: New Collection</p> <p>1.	The system takes the user to a new screen that displays a search bar, a blank title box where the game title must be entered, a �+� button on top of the title box, and 2 buttons on the side of it (�+� and �- �). When the inputs the game title, the use case proceeds.</p> <p>S-3: Category Box</p> <p>1.	The system displays a category box that has 2 buttons on the top of it (�+� and �- �) and 4 buttons on the side of it (�+�, �- �, �C�, �I�) to add or remove sub-categories and add comments or images.</p>  |
|<p>**Alternate(A) / Exceptional (E) Flows:**<p/> <p>NF-A1: At NF1 the user selects the �Community Collection� button.<p/> <p>NF-A2: At NF3 the user selects an item from displayed collections.<p/> <p>NF-A3: At NF3 the user searches in the current collection screen.<p/> <p>NF-E1: Any errors that occurred during NF including sub-flows end the use case.<p/> <p>NF-E2: At NF3 S-2 the user jumps to select title box �+� button should signify user to input title before adding category box and return to the beginning state of S-2.<p/> |

----

## User Removes Category
|   			|  		
| ------------- | 	
| <p>**Use Case Name:** User Removes Category</p>	 <p>**ID:** 15</p>	<p>**Importance Level:** High<p> <p>**Use Case Type:**	System </p>  |
| **Primary Actor:**	User |	 
| **Stakeholders and Interests:** Primary Actor � Gamer interested in keeping track of game information. |  
**Brief Description:** The user removes category use case defines the process of the user removing categories from the collection that is being worked on.  |
| <p>**Trigger:**	Clicked GUI �-� button.</p> <p> **Type:** Button Event</p> |  
|<p>**Relationships:**</p> <p>Association: User removes category</p> <p>Include: User removes sub-category</p> <p>Extend: User removes text, User removes image</p> <p>Generalization: User edits single collection</p> | 
|<p>**Normal Flow (NF) of Events:**</p> <p> 1. The use case begins with the application in the main menu and the user has two options to click from: �Current Collection� or �Community Collection�.</p> <p> 2. The user clicks from �Current Collection�; the S-1 Collection Screen sub-flow is performed. the S-1: Collection Screen sub-flow is performed.</p> <p> 3. The user clicks on one item listed from the current collection; the S-2 Item View sub-flow is performed.</p> <p> 4. The user selects the edit button; the S-3 Edit View sub-flow is performed.</p> <p>5. The user clicks the top �-� button from one displayed category; the S-4 Category Remove Action sub-flow is performed.</p> |
|<p>**Sub Flows:**</p> <p>S-1: Collection Screen</p> <p>1. The system displays all the user�s current collections. The use case proceeds. </p> <p>S-2: Item View</p> <p>1. The system shows all previous components (Title box, category, sub-categories, images, comments) that were placed into this collection. The use case proceeds.</p> <p>S-3: Edit View</p> <p>1. The system shows the user the edit mode showing �+�, �-� or �C�, �I� on category and sub-category components. The use case proceeds.</p> <p>S-4: Category Remove Action</p> <p>1. The system removes the category that the user clicked the �-� button on, and the remaining screen components are displayed.</p> |
|<p>**Alternate(A) / Exceptional (E) Flows:**<p/> <p>NF-A1: At NF1 the user selects the �Community Collection� button.<p/> <p>NF-A2: At NF3 the user selects the �+� button in the displayed collections screen to add a new collection.<p/> <p>NF-A3: At NF3 the user searches in the current collection screen.<p/> <p>NF-A4: At NF5 the user enters information on the search bar.<p/> <p>NF-E1: Any errors that occurred during NF including sub-flows end the use case.<p/> <p>NF-E2: At NF5 the user selects the �+� button on any category or sub-category the use case ends.<p/> <p>NF-E3: At the NF-A4 alternate step, the user proceeds to search the use case ends.<p/> |



----

## User Publishes Collection

| 			|  		
| ------------- | 	
| <p>**Use Case Name:**  User Publishes Collection </p>	 <p>**ID:** 14</p>	<p>**Importance Level:** High<p> <p>**Use Case Type:**	System </p>  |
| **Primary Actor:**	User |	 
| **Stakeholders and Interests:** Primary Actor � interested in keeping track of game information. |  
**Brief Description:** The user publishes collection use case defines the process of the user saving on the cloud database for community viewing. |
| <p>**Trigger:** Clicked GUI �Publish� button.</p> <p> **Type:** Button Event</p> |  
|<p>**Relationships:**</p> <p>Association: User publishes collection</p> <p>Include: Add to database collection</p> <p>Extend: User saves collection</p> <p>Generalization: User edits single collection</p> | 
|<p>**Normal Flow (NF) of Events:**</p> <p> 1.	This use case begins at the main menu and the user selects �Current Collection�; an S-1 Collection Screen sub-flow is performed.</p> <p>2. The user selects a single item from the current collection; the S-2 Item View sub-flow is performed.</p> <p>3. The user clicks the edits collection button; the S-3 Edit View sub-flow is performed.</p> <p>4. The user clicks on the �Publish� button; the S-4 User Confirmation sub-flow is performed.</p> <p>5. The user clicks �Yes� on the confirmation window; the S-5 Publish Complete sub-flow is performed.</p>|
|<p>**Sub Flows:**</p> <p>S-1: Collection Screen</p> <p>1.	The system displays all the user�s current collections.</p> <p>	S-2: Item View </p> <p>1. The system shows all previous components (Title box, category, sub-categories, images, comments) that were placed into this collection.</p> <p>S-3: Edit View</p> <p>1. The system shows the user the edit mode where �+�, �-� or �C�, �I� are on category, subcategory components.</p> <p>S-4: User Confirmation</p> <p> 1.	The system shows the user a separate confirmation window to proceed with publishing. <p>S-5: Publish Complete</p> <p>1. The system notifies the user that publishing was completed.</p> |
|<p>**Alternate(A) / Exceptional (E) Flows:**<p/> <p>NF-A2: At NF1 the user exits the application.<p> <p>NF-A3: At NF2 the user searches on the screen.</p> <p>NF-A4: At NF4 the user clicks the �Save� Button.</p> <p>NF-E1: Any errors that occurred during NF including sub-flows end the use case.</p> <p>NF-E2: At NF-A2 and NF-A3 ends the use case.</p> <p>NF-E3: At NF-A4 alternate step if the user clicks the �Save� button, return to NF2-S-2, and proceed with the use case.</p> |


