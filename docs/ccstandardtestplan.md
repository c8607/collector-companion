# **Standard Test Plan**


# Table of Contents

- 1 **[Test Plan](#1-test-plan)**
  - 1.1 [Test Plan Identifier](#test-plan-identifier )
  - 1.2 [Introduction](#introduction)
  - 1.3 [Test Items](#test-items)
  - 1.4 [Features to be Tested](#features-to-be-tested)
  - 1.5 [Features not to be Tested](#features-not-to-be-tested)
  - 1.6 [Approach](#approach)
  - 1.7 [Items Pass Fail Criteria](#items-pass-fail-criteria)
  - 1.8 [Suspension Criteria and Resumption Requirements](#suspension-criteria-and-resumption-requirements)
  - 1.9 [Test Deliverables](#test-deliverables)
  - 1.10 [Test Task](#test-task)
  - 1.11 [Environmental Needs](#environmental-needs)
  - 1.12 [Responsibilities](#responsibilities)
  - 1.13 [Staffing and Training Needs](#staffing-and-training-needs)
  - 1.14 [Schedule](#schedule) 
  - 1.15 [Risks and Contingencies](#risks-and-contingencies) 
  - 1.16 [Approvals](#approvals)
       
       
# 1 Test Plan
# Test Plan Identifier        
CC_0001
<Version 1.0>
 5/14/2022


|  Date: 		| Revision: 	|  Description: 				|  Author: 			|
| ------------- | ------------- | ------------- 				| ------------- 	|
| <5/14/2022> 	| 1.0 			|  Initial draft 				| Kimberly Hernandez|
| <5/15/2022> 	| 1.1 			|  Continued adding to draft	| Kimberly Hernandez|
| <6/9/2022> 	| 1.2  			|  Transferred MS document info to MD document | Kimberly Hernandez|
| <6/11/2022> 	| 1.2  			|  Edited document indentations | Kimberly Hernandez|


# Introduction

This test plan document is the master plan for the Collector Companion project. The Collector Companion will allow the gaming community to add the findings of their favorite game on this application, track what they have found, publish it for community use, and be able to search other collections. To begin the test plan of the project, adding a category, removing a category, publishing, selecting, and searching a collection will be tested. The scope of testing efforts remains in these sections. Other documentation that may be referenced throughout for requirements and recommendations are the [Business Problem Scenario](./docs/ccbusinessproblemscenario.md) and [Software Requirements Specification (SRS)](./docs/ccsrs.md). 

# Test Items

<ins>The following items will be tested:</ins>

- Application to respond to user add request when adding a GUI component (representing a category) to an item (collection).
- Application to respond to users remove requests when removing a GUI component (representing a category) from the item (collection).
- Application to respond to user requests and submit and save item(collection) to the database.
- Application to respond to users' search requests for specific items (collection) using the database.
- Application to respond to user requests when clicking for a single item(collection) of multiple items(collections). 



# Features to be Tested

<ins>The following features to be tested are presented with a level of risk (H -High, Medium, L-Low).</ins>

- Use the graphical user interface to add a category to a collection. (H)
- Use the graphical user interface to remove a category from the collection. (H)
- Use the graphical user interface to publish a Collection. (H)
- Search through community collections. (H)
- Use the graphical user interface to select a single community post. (H)


# Features not to be Tested

- Add subcategory feature. This is like adding a category feature which makes this a low-risk feature.
- Remove subcategory feature. This is like removing the category feature which makes this a low-risk feature.
- Add text feature. This has been implemented in a previous project with no issues in development and testing and is considered a low-risk feature.
- Add image feature. This has been implemented in a previous project with no issues in development and testing and is considered a low-risk feature.
- Remove text feature. This has been implemented in a previous project with no issues in development and testing and is considered a low-risk feature.
- Remove image feature. This has been implemented in a previous project with no issues in development and testing and is considered a low-risk feature.
- The select checkbox in a collection. Will be a functional part of the release and is considered a low-risk/stable feature.
- Rate collection. Will be a functional part of the release and is considered a low-risk/stable feature.


# Approach

<ins>Tools needed for feature testing are:</ins>

   - Microsoft Excel to fill out test documentation. No training is needed for this tool
   - Eclipse Integrated Development Environment will be used for application testing.<dd> - The Eclipse resource will be used by the developer.</dd> <dd> - The developer is responsible for testing GUI components based on Class methods.</dd>

 These tools may limit testing if they are not accessible on hardware. See *[SRS section 3.1 External Interface Requirements](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#external-interface-requirements)* for more information on hardware and software requirements.

 Metrics measure feature functionality based on the combination of GUI components and class methods.

 Deadlines for testing are during the implementation phase of the project and must be completed by 6/14/2022.

 Regression testing will take place when changes are made based on testing results, documentation is reviewed, and the severity of the feature as it impacts the overall application.


# Items Pass Fail Criteria

<ins>All the functional requirement test items below should be completed and met the listed criteria:</ins>

- Application to respond to users add requests when adding a GUI component (representing a category) to an item (collection). 
  - Code is reviewed for GUI component functionality.
  - No application crashes.
  - Testing is completed when 80% of cases are completed with 20% minor defects.

- Application to respond to users remove request when removing a GUI component (representing a category) from the item (collection).
  - Code is reviewed for GUI component functionality.
  - No application crashes.
  - Testing is completed when 80% of cases are completed with 20% minor defects.

- Application to respond to users' requests to submit and save items (collection) to the database.   
  - Code is reviewed for GUI component functionality.</dd>
  - Database connection reads as part of completed cases and is not a defect.
  - No application crashes.
  - Testing is completed when 80% of cases are completed with 20% minor defects.

- Application to respond to a user's search request for a specific item (collection) using the database.
  - Database connection reads as part of completed cases and is not a defect.
  - No application crashes.
  - Testing is completed when 80% of cases are completed with 20% minor defects.

- Application to respond to user requests when clicking for a single item(collection) of multiple items(collections).
   - Database connection reads as part of completed cases and is not a defect.
  - Code is reviewed for GUI component functionality.
  - No application crashes.
  - Testing is completed when 80% of cases are completed with 20% minor defects.

# Suspension Criteria and Resumption Requirements

- Test criteria should be suspended once testing reaches more than 80% completion of cases for each test item.
  - This includes test item GUI component functions properly.
  - Examples of defects that pass test suspension include the visibility of GUI components (large or small).

- Another suspension case would be other errors that occur not part of this test plan.
  - Examples of these are errors that stem from features that are not being tested.

- Testing criteria should resume if more than 20% of defects remain for each test item.
  - This includes application crashes.
  - Includes GUI components that are positioned off-screen or inaccessible to the user.
  - The user is not able to access the database.


# Test Deliverables

The data retrieved from the test plans and excel sheet will deliver a Test Summary Report.

# Test Task

- For an end-user a windows computer will be needed, see *Software Requirements Specification* on 
*[3.1 External Interface Requirements](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#external-interface-requirements)*, to start the application and to start testing the features of the application. Basic skill levels of applications and computer functions are needed to perform the test. As each step is performed Microsoft Excel will be used to take information on defects or successful completion of testing steps.

- For developers, Eclipse, Excel, and PC will be used to run the application and take information on testing defects and successful completion of testing steps. Proficient to Expert skill levels in applications and computer functions are needed to perform the tests. As each step is performed Microsoft Excel will be used to take information on defects or successful completion of testing steps. 

See sections [Items Pass/Fail Criteria](#items-pass-fail-criteria) and [Suspension Criteria and Resumption Requirements](#suspension-criteria-and-resumption-requirements) for additional information regarding testing limits.



# Environmental Needs

<ins>Environmental needs vary based on the hardware used, the following list is needed to proceed with test plans:</ins>

- Feedback on test data will be provided through Microsoft Excel to create a Final/UpdatedTest.xslx report.
- Only the items listed in this document are the restrictions to testing.
- Reliable network connections such as LAN, Wi-fi, and VPN configurations are acceptable for application testing and vary greatly depending on the users set-up.
- A firewall is recommended but not required for testing; will depend on the tester's set-up
- Computer screens are needed to display the application. 
- A mouse and keyboard are needed to navigate the user interface and test the application.
- Versions of Microsoft Excel, Eclipse, and OS should be up to date.

Recommendations for this section are listed in the *Software Requirements Specification (SRS) section [3. Specific Requirements](https://gitlab.com/c8607/collector-companion/-/blob/main/docs/ccsrs.md#3-specific-requirements)*.


# Responsibilities

<ins>Kimberly Hernandez, developer, will be responsible for:</ins>

- Each person takes part in the testing/implementation phase. This also includes all aspects of the testing process and items delivered for testing.
- Testing limits and restrictions that are listed in the sections above will be in order during the testing/implementation phase and should be done by 6/14/2022.
- No training is required for testing.


# Staffing and Training Needs

No training is required unless any party is unfamiliar with Microsoft Excel & OS or Eclipse Integrated Development Environment. This also includes a basic understanding of computer application use.

# Schedule

Testing will take place from 6/9/2022  6/14/2022. If software construction is finished before 6/9/2022 testing schedule will change and begin on 6/8/2022 and finish on 6/13/2022. On 6/13/2022 testing summaries from the Test Summary Report will be created to move on with the maintenance task of the project plan. No room left for further testing before or after these dates.

# Risks and Contingencies

<ins>Risks include:</ins>

- Resources are not available during the testing phase causing delays.
- Lack of knowledge of tools required for testing.
- Delays in software and construction tasks in the implementation phase cause delay in testing and limit time.
- Changes in previous planning documentation cause prior tasks to take longer and delay testing.

To handle the events above:
- The scope will be revisited to see what can be done to keep the schedule on track.
- Other testing resources will be gathered for the testing/implementation phase if the budget holds well.
- Reconsider features to be tested if changes are made to scope.
- Leave additional time for phases that may encounter more risks to prevent testing delays.

# Approvals

<ins>To determine when the testing process is complete approvals must take place and be 
reviewed before the maintenance phase is reached. Approvals needed:</ins>

- On a master plan level from the project manager and main stakeholders that are involved in the development of the project scope.
- On test plan documentation from project management.
- By programmers for feature delivery and testing information.
- By developers or other testers for the testing process.



