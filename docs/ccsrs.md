# **Software Requirements Specification**

# **Revision History**
|  Date: 		| Revision: 	|  Description: 				|  Author: 			|
| ------------- | ------------- | ------------- 				| ------------- 	|
| <5/7/2022> 	| 1.0  			|  Initial draft	  			| Kimberly Hernandez|
| <5/14/2022> 	| 1.1  			|  Updated TOC for additional functional requirements| Kimberly Hernandez|
| <5/15/2022> 	| 1.2  			|  Updated section 4. Other Nonfunctional Requirements			| Kimberly Hernandez|
| <6/9/2022> 	| 1.3  			|  Transferred MS document info to MD document | Kimberly Hernandez|
| <6/11/2022> 	| 1.4  			|  Edited document & added quick links | Kimberly Hernandez|
| <6/11/2022> 	| 1.5  			|  Edited UI Alerts & Save Community Collection functional requirements | Kimberly Hernandez|


# Table of contents

- 1. **[Introduction](#1-introduction)**
  - 1.1 [Purpose](#purpose)
  - 1.2 [Scope](#scope)
  - 1.3 [Definitions, Acronyms, Abbreviations, Document Conventions](#definitions-acronyms-abbreviations-document-conventions)
  - 1.4 [References](#references)
  - 1.5 [Overview](#overview)
- 2. **[Overall Description](#2-overall-description)**
  - 2.1 [Product Perspective](#product-perspective)
  - 2.2 [Product Functions](#product-functions)
  - 2.3 [User Characteristics](#user-characteristics)
  - 2.4 [Constraints](#constraints)
  - 2.5 [Assumptions and Dependencies](#assumptions-and-dependencies)
- 3. **[Specific Requirements](#3-specific-requirements)**
   - 3.1 [External Interface Requirements](#external-interface-requirements)
   	 - 3.1.1 [User Interface](#user-interface)
   	 - 3.1.2 [Hardware Interfaces](#hardware-interfaces)
   	 - 3.1.3 [Software Interfaces](#software-interfaces)
   	 - 3.1.4 [Communication Interfaces](#communication-interfaces)  	   
  - 3.2 [Functional Requirements](#functional-requirements)
   	 - 3.2.1 [Subsystem - Application startup/GUI](#subsystem-application-startup-gui)
   	 - 3.2.2 [Subsystem - Select from Home GUI](#subsystem-select-from-home-gui) 
   	 - 3.2.3 [Subsystem - Search from Collections](#subsystem-search-from-collections)
   	 - 3.2.4 [Subsystem - UI shows list of collections](#subsystem-ui-shows-list-of-collections) 
   	 - 3.2.5 [Subsystem - Editing options](#subsystem-editing-options)
   	 - 3.2.6 [Subsystem - Add images](#subsystem-add-images)
   	 - 3.2.7 [Subsystem - Add comments](#subsystem-add-comments)
   	 - 3.2.8 [Subsystem - Apply signature name](#subsystem-apply-signature-name)  	    
   	 - 3.2.9 [Subsystem - UI Post preview](#subsystem-ui-post-preview)
   	 - 3.2.10 [Subsystem - Update text options](#subsystem-update-text-options)
   	 - 3.2.11 [Subsystem - Update comments](#subsystem-update-comments)
   	 - 3.2.12 [Subsystem - Checkbox option](#subsystem-checkbox-option) 	   
   	 - 3.2.13 [Subsystem - Local save option](#subsystem-local-save-option)
   	 - 3.2.14 [Subsystem - Cloud save option](#subsystem-cloud-save-option)
 	 - 3.2.15 [Subsystem - Add or drop categories](#subsystem-add-or-drop-categories)  	    
   	 - 3.2.16 [Subsystem - Rate post option](#subsystem-rate-post-option)  	   
   	 - 3.2.17 [Subsystem - UI alerts](#subsystem-ui-alerts)
   	 - 3.2.18 [Subsystem - Selecting community collections](#subsystem-selecting-community-collections)
   	 - 3.2.19 [Subsystem - Save Community Collection](#subsystem-save-community-collection)
- 4. **[Other Nonfunctional Requirements](#4-other-nonfunctional-requirements)**
  - 4.1 [Performance Requirements](#performance-requirements)
   	 - 4.1.1 [Standards](#standards)
   	 - 4.1.2 [Hardware Limitations](#hardware-limitations)
  - 4.2 [Design Constraints](#design-constraints)
   	 - 4.2.1 [Availability](#availability)
   	 - 4.2.2 [Functional Suitability](#functional-suitability)
   	 - 4.2.3 [Maintainability](#maintainability)
   	 - 4.2.4 [Reusability](#reusability)
   	 - 4.2.5 [Security](#security)
   	 - 4.2.6 [Usability](#usability)
  - 4.3 [Software System Attributes](#software-system-attributes)
- 5. **[Other Requirements](#5-other-requirements)**

# 1. Introduction
# Purpose

The Software Requirements Specifications (SRS) for Collector Companion, a project initiated by Start-up Software, are covered below for version 1.0. Updated information to the SRS will be listed in the [Revision History](#revision-history) section when additional documentation is added. The SRS will provide the scope, overall description, and specific requirements as the first part of the document. It is intended for the stakeholders, project manager, and developers of the Collector Companion project. The Collector Companion project was initiated for developers and the gamming community to create an application that allows users to track game information and in turn provide developers with feedback for future developments. 

# Scope

Collector Companion will allow gamers to continue their goals, challenges, and collection of in-game items all in one place. The application will bring flexibility to the end-user. End-users will be able to track multiple favorite games, add categories and subcategories, add images for visual pointers, and comments for additional information or export to other sources. The end-user will be able to save or delete their game collections for the rest of the community to explore. 

# Definitions, Acronyms, Abbreviations, Document Conventions

<ins>Cloud Database</ins> 

Collection of information stored in a cloud environment and is managed based on a structured query language.

<ins>Database</ins> 

Collection of information in the application or system that is managed based on a structured query language.

<ins>End-user or user</ins> 

A person who uses the final release of the application for entertainment or to enhance productivity.

<ins>FTP (File Transfer Protocol), Hypertext Transfer Protocol Secure (HTTPS)</ins>

Types of internet protocols.

<ins>Graphical User Interface (GUI)</ins>

Components that make up the application and that allow the user to easily interact with the application.

<ins>Operating System (OS)</ins>

An operating system that is installed into personal owned devices that allow computer functionality. 

<ins>Personal Computer (PC)</ins>

Personally owned desktop or laptop computer. 

<ins>Replayability</ins>

Referencing a video game and repeating game play based on extensive game quality.
	
<ins>User Interface</ins>

Displays present on graphical user interface or application.

Bold formats and identification numbers are used to identify headings and subheadings in the documentation. To distinguish versions of OS and minimum requirements of systems italicized formats are used. 

# References

[1] "IEEE Recommended Practice for Software Requirements Specifications," in IEEE Std 830-1998, vol., no., pp.1-40, 20 Oct. 1998, Doi: 10.1109/IEEESTD.1998.88286.

[2] Tutorials Point. (n.d.). Software testing � iso standards. https://www.tutorialspoint.com/software_testing/software_testing_iso_standards.htm

# Overview

This document presents the Collector Companion as a stand-alone product. In addition, the document was prepared in the reference above following EEE std. 830-1008 SRS format.

# 2. Overall Description
# Product Perspective

In today�s gaming world challenges continue for developers to bring their communities more features to enjoy. Players continue to expect these exciting features to bring more goals, challenges, or even areas to explore in their favorite games. To keep the gamer close to the community and many features of their favorite games, Collector Companion is an application idea that aims to do so. The project was initiated by playing a variety of games and returning to them because of their replayability, but there is a disconnect between the communities that explore and share what they have found in their game playthrough. These games have a vast number of features allowing a percentage of the gaming community to break down the game and show what was found in the game through social media. But social media continues to constantly update losing this information, and with guidebooks being off the shelves for years now, this application will allow the end-user to have their Collector Companion available to them at any time to truly reconnect them to their game and community. 

# Product Functions

- Ability to fully customize based on the user�s choice of game. 
- A menu option for users� current collection of games they are working on.
- A search menu option for the collection of community posts. 
- Display users' current collection in list form by game title name. 
- Ability to save, edit, delete, or publish.
- Ability to add images for sub-category item visual aids. 
- Ability to add comments to sub-category items. 
- Allow the end-user to apply a signature name. 
- Ability to update titles, categories, and sub-categories text, comments, checkboxes, images, and user signature name features.
- Ability to view current work before proceeding to edit, save, delete, or publish options.
- Allow users to save locally or save on the cloud for community viewing.
- Provided a drop-down menu to show sub-items listed in main categories. 
- Sub-categories of items provided a check box.
- Allow the user to use a search bar for game information or keywords.
- Searches filter down by game or user signature name with the most likes at the top.
- Allow all users to rate posts by clicking on a like button.
- Internet access allows other users to search from published posts.
- Allow download for PC running Microsoft Windows OS.

# User Characteristics

The general user that is 13+, knows Windows PC technologies including knowledge of video games to build on application information. The general use should also have knowledge of basic application features like buttons, textboxes, and editing options. 

# Constraints

- No internet access only allows local application features.
- Touch tablet features disabled during application use.
- Only available for Windows PC users.
- Eclipse Integrated Development Environment and MySQL Workbench needed for project completion and application development. 

# Assumptions and Dependencies

Assumptions and dependencies when using application:
   - Windows OS is used before application download or in application use.
   - End-user has access or able to install application on Windows PC.
   - End-user has enough storage for application.
   - Has correct dependencies such as OS and hardware specifications as listed in those sections. See *[Specific Requirements](#3-specific-requirements) section of SRS*.
   - Internet access by wired or wireless connection on PC to allow community search feature.
   - User will apply visual aids in image formats and not video formats.

# 3. Specific Requirements
# External Interface Requirements

The application will bring flexibility to the end-user. End-users will be able to add categories, subcategories, images for visual pointers, and comments for additional information or export to other sources to keep track of their game. The end-user will also be able to save or delete their game collections.

# User Interface

All the following are needed for application:
- Desktop or laptop running Microsoft Windows to download application.
   - Windows OS:
   	 - *10 Home version 21H2 or newer releases and Architecture x32, x64*
- Working desktop or laptop monitor for displaying application to end-user.
- Application views are in window form and sized accordingly to monitor screen size.

# Hardware Interfaces

- Mouse and keyboard required for Windows PC to navigate application GUI.

- PC resolution screen can run the application on *1280p x 2024p � 1920p x 1200p* resolutions. 

# Software Interfaces

� No other programs needed to enable additional features of the application.

# Communication Interfaces

Communication protocols needed:

- IPV4 or IPV6 for PC and application internet connection.
- FTP for file sharing between PCs.
- HTTPS to access to external links.

# Functional Requirements

01 � The user interface shall show the user's current user game collection or community collection on the main screen.

02 � The application shall allow the user to select the current collection or to search from community posts.

03 � The application will allow the user to search local collections or community collections.

04 � The user interface shall show the user collections in list form by game title.

05 � The application will allow the user to save, edit, or delete their game collection. 

06 � The application will allow the user to add images to the category or sub-category items added.

07 � The application will allow the user to add comments to category and sub-category items.

08 � The application shall allow the user to apply a signature name to the end of the post.

09 - The user interface will show the added categories, sub-categories, comments, and images before saving, publishing, or deleting.

10 � The application shall allow the user to update categories and sub-category text boxes.

11 � The application shall allow the user to update categories and sub-category comments using text boxes.

12 � The application shall allow the user to check mark boxes for items indicating completion.

13 � The application shall allow the user to save locally for personal use.

14 - The application shall allow the user to save on the cloud for community viewing. 

15 - The application shall allow the user to add or drop categories and sub-categories by clicking the GUI buttons.

16 - The application shall allow the user to rate posts by clicking a heart button indicating it was liked otherwise no rate is given.
 
17 � The user interface will alert the user for empty category or sub-category items, and save, publish, add, delete yes or no options.

18 � The application shall allow the user to select a community post.

19 - The application shall allow the user to save a community post.

# Subsystem - Application startup GUI

Requirements Statement: 

Start of application shows GUI and user�s current game collection or community collection search sections.

# Subsystem - Select from Home GUI

Requirements Statement:

User to select from the current game collection or community collection search.

# Subsystem - Search from Collections

Requirements Statement:

User to search local game collection or community collection.

# Subsystem - UI shows list of collections

Requirements Statement: 

UI shows end-user current game collection or community collection in list form.

# Subsystem - Editing options

Requirements Statement: 

End-user will save, edit, or delete their current game collection.


# Subsystem - Add images

Requirements Statement: 

End-user adds images to category or sub-category items by clicking image button.


# Subsystem - Add comments

Requirements Statement: 

User to selects comments button from category or subcategory items to add comment to them.


# Subsystem - Apply signature name

Requirements Statement: 

User adds signature to end of active window before saving, editing, or publishing post.

   	    
# Subsystem - UI Post preview

Requirements Statement: 

User interface displays active window that includes added categories, sub-categories, comments, and images. 


# Subsystem - Update text options

Requirements Statement:

During editing, user can update categories and subcategory text boxes.


# Subsystem - Update comments

Requirements Statement:

During editing, user can update categories and sub-category to add comments.

# Subsystem - Checkbox option

Requirements Statement:

User selects checkbox indicating item completion.

   	   
# Subsystem - Local save option

Requirements Statement:

User selects save from editing option to save locally. 


# Subsystem - Cloud save option

Requirements Statement:

User selects publish from editing option to save to the cloud.


# Subsystem - Add or drop categories
   	    
Requirements Statement:

User selects from plus or minus buttons to add or drop categories. 
   	    
   	    
# Subsystem - Rate post option

Requirements Statement:

User selects like button to rate community post that is being viewed.

   	   
# Subsystem - UI alerts

Requirements Statement:

Application alerts user with a new window pop-up indicating empty category or subcategory items, and save, delete, publish, or add yes or no options.


# Subsystem - Selecting community collections

Requirements Statement:

The application lets the user to select a single community post to view.

# Subsystem - Save Community Collection

Requirements Statement:
The application lets the user view and save a community collection to their local collections.


# 4. Other Nonfunctional requirements

# Performance Requirements
# Standards
ISO/IEC 9126 for ensuring quality standards.

Software quality response times of Collector Companion will vary based on end-users Windows PC and other varying factors that are not listed in the [Specific Requirements](#3-specific-requirements) section of SRS. Loading screen will respond 2 seconds after application is started on Windows PC. 

# Hardware Limitations

- Hardware limitations are based off the [Specific Requirements](#3-specific-requirements) section of the SRS.
- Average *network ping times of 100ms* and below for search features of application.
- Varying graphics cards may limit application performance and usability. 

# Design Constraints
# Availability

- Eclipse Integrated Development Environment (IDE) needed to program application. 
- MySQL Workbench needed to program database, implement queries for data retrieval, and save for local application use.

# Functional Suitability

- The application requires functional completeness, correctness, and appropriateness to meet overall standards and specifications.

# Maintainability

- This application requires updated Windows OS and storage for optimal use and for future updates.
- Internet connection required for future updates.

# Reusability

- Eclipse Integrated Development Environment needed for object-oriented programming to reuse code and methods at no cost to time.

# Security

- Protocols implemented for security based on section [3.1.4 Communication Interfaces](#communication-interfaces) and encryption will be used on all user-sensitive information.
- Database hacking prevention needed by using Eclipse and including prepared statements.
- End-user OS firewall.

# Usability

- Microsoft Visio will be needed to initiate application environment design.
- Eclipse Integrated Development Environment program needed to create graphical user interface functionality and navigation.
- The application environment is expected to ensure easy navigation for end-user throughout application life. 

# Software System Attributes

Based on section [4.2 Design Constraints](#design-constraints) the following attributes are important for the application:

� Application [availability](#availability) is important overall to keep the application and data running for the end-user. The IDE and MySQL software is needed for code implementation for any defects that may occur after application release and handle the input and output of the application data.
 
� [Functional Suitability](#functional-suitability) is important for application development and for stakeholders� expectations of specifications listed in requirements and non-requirements of the application. 

� Application [maintainability](#maintainability) is important to give the end-user updates in case any errors or defects occur post development. When pushing updates, the user�s internet connection, updated Windows OS, and storage allows the developers to maintain the application without disrupting end-user experience.

� [Reusability](#reusability) is important to re-use code and methods that can be done through Eclipse IDE which can save time for developers and planning as well as help find errors or defects if they occur.

� Application [usability](#usability) is important to make sure the end-user has a smooth experience with the graphical user interface. Smooth experience by the end-user entails navigation through each GUI component that ranges from availability to functionality of components. 

# 5. Other Requirements

� Internet service provider required for online search feature.

� MySQL Workbench required for developers to implement local testing and store data during development. 

� Cloud database required to transfer data for production release and for online search feature of application.
